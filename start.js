const express = require('express');
const createError = require('http-errors');

const app = express();

app.use(express.json());

const database = [];

app.get('/rutaGet', (req, res, next) => {
    res.send('Hello world!');
});

app.get('/rutaGetCuParametru/:param1/:param2', (req, res, next) => {
    const {
        param1,
        param2
    } = req.params;

    res.send(`Ai trimis parametri ${param1} si ${param2}`);
});

app.post('/rutaDeTipulPost', (req, res, next) => {
    //presupunem ca in corp sunt elementele titlu si autor
    const {
        titlu,
        autor
    } = req.body;

    if (!titlu) {
        next(createError(400, 'Nu ai inclus elementul titlu!'));
    }
    
    if (!autor) { 
        next(createError(400, 'Nu ai inclus elementul autor!'));  
    }
    res.status(201).json({
        message: 'Ai trimis urmatoarele elemente:',
        titlu,
        autor
    });
});

app.post('/rutaDeTipulPostFaraRetur', (req, res, next) => {
    const {
        titlu,
        autor
    } = req.body;

    database.push({titlu, autor});

    res.status(201).end();
});

const port = process.env.PORT || 3000; //daca aveti setat port in variabila de mediu, va lua valoarea lui

app.listen(port, (err) => {
    if (err) {
        console.error(err);
    } else {
        console.info(`Server is listening on port ${port}`);
    }
});